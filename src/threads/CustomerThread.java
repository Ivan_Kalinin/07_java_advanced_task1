package threads;

import program.HotelBookingRequest;
import program.LimitedQueue;

public class CustomerThread extends Thread {

	private LimitedQueue limitedQueue;
	private HotelBookingRequest hotelBookingRequest;

	public CustomerThread(LimitedQueue limitedQueue,
			HotelBookingRequest hotelBookingRequests) {
		this.limitedQueue = limitedQueue;
		this.hotelBookingRequest = hotelBookingRequests;
	}

	@Override
	public void run() {
		try {
			limitedQueue.put(this.hotelBookingRequest);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
