package threads;

import program.HotelBookingRequest;
import program.LimitedQueue;

public class BookerThread extends Thread {
	private LimitedQueue limitedQueue;
	private HotelBookingRequest hotelBookingRequest;

	public BookerThread(LimitedQueue limitedQueue) {
		this.limitedQueue = limitedQueue;
	}

	public HotelBookingRequest getHotelBookingRequests() {
		return hotelBookingRequest;
	}

	public void setHotelBookingRequests(
			HotelBookingRequest hotelBookingRequest) {
		this.hotelBookingRequest = hotelBookingRequest;
	}

	@Override
	public void run() {
		try {
			this.hotelBookingRequest = limitedQueue.get();
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
