package program;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class LimitedQueue {
	private static final Logger log = LogManager.getLogger(LimitedQueue.class);;

	private List<HotelBookingRequest> limitedQueue = new LinkedList<HotelBookingRequest>();

	public List<HotelBookingRequest> getLimitedQueue() {
		return limitedQueue;
	}

	public void setLimitedQueue(LinkedList<HotelBookingRequest> limitedQueue) {
		this.limitedQueue = limitedQueue;
	}

	private int limit;

	public LimitedQueue(int limit) {
		this.limit = limit;
	}

	public synchronized void put(HotelBookingRequest queueElement)
			throws InterruptedException {
		log.info(queueElement.getCustomerName() + ": sent");
		while (this.limitedQueue.size() == this.limit) {
			wait();
		}

		if (this.limitedQueue.size() == 0) {
			notifyAll();
		}

		Thread.sleep(5000);
		
		this.limitedQueue.add(queueElement);
		log.info(queueElement.getCustomerName() + ": received");
	}

	public synchronized HotelBookingRequest get() throws InterruptedException {
		while (this.limitedQueue.size() == 0) {
			wait();
		}

		if (this.limitedQueue.size() == this.limit) {
			notifyAll();
		}

		HotelBookingRequest bookingRequest = this.limitedQueue.remove(0);
		log.info(bookingRequest.getCustomerName() + ": processed");
		return bookingRequest;
	}
}
