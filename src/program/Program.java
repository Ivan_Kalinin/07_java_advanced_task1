package program;

import java.time.LocalDate;
import java.time.Month;

import threads.BookerThread;
import threads.CustomerThread;

public class Program {

	public static void main(String[] args) {
		// request flow
		LimitedQueue limitedQueue = new LimitedQueue(3);

		HotelBookingRequest request1 = new HotelBookingRequest(1, "customer1",
				LocalDate.of(2006, Month.DECEMBER, 1), "hotel 12", 1);
		HotelBookingRequest request2 = new HotelBookingRequest(2, "customer2",
				LocalDate.of(2006, Month.APRIL, 4), "hotel 12", 33);
		HotelBookingRequest request3 = new HotelBookingRequest(3, "customer3",
				LocalDate.of(2007, Month.FEBRUARY, 11), "hotel 4", 344);
		HotelBookingRequest request4 = new HotelBookingRequest(4, "customer4",
				LocalDate.of(2008, Month.DECEMBER, 2), "hotel 3", 45);
		HotelBookingRequest request5 = new HotelBookingRequest(55, "customer5",
				LocalDate.of(2007, Month.FEBRUARY, 12), "hotel 12", 345);
		HotelBookingRequest request6 = new HotelBookingRequest(134,
				"customer6", LocalDate.of(2008, Month.DECEMBER, 22),
				"hotel 11", 15);

		// customers
		CustomerThread[] customerThreads = new CustomerThread[] {
				new CustomerThread(limitedQueue, request1),
				new CustomerThread(limitedQueue, request2),
				new CustomerThread(limitedQueue, request3),
				new CustomerThread(limitedQueue, request4),
				new CustomerThread(limitedQueue, request5),
				new CustomerThread(limitedQueue, request6) };

		// bookers
		BookerThread[] bookerThreads = new BookerThread[] {
				new BookerThread(limitedQueue), new BookerThread(limitedQueue),
				new BookerThread(limitedQueue) };

		// starts
		for (BookerThread thread : bookerThreads) {
			thread.start();
		}
		for (CustomerThread thread : customerThreads) {
			thread.start();
		}
	}

}
