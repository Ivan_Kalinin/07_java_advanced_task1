package program;

import java.time.LocalDate;

public class HotelBookingRequest {
	private int id;
	private String customerName;

	private LocalDate date;
	private String hotelName;
	private int room;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public int getRoom() {
		return room;
	}

	public void setRoom(int room) {
		this.room = room;
	}

	public HotelBookingRequest(int id, String customerName, LocalDate date,
			String hotelName, int room) {
		this.id = id;
		this.customerName = customerName;
		this.date = date;
		this.hotelName = hotelName;
		this.room = room;
	}
}
